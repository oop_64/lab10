package com.thanakit;

public class Triangle extends Shape {
    private double a;
    private double b;
    private double c;

    public Triangle(double a, double b, double c) {
        super("Triangle");
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public double calArea() {
        // TODO Auto-generated method stub
        double s = ((a + b + c) / 2);
        return Math.sqrt(s * (s - a) * (s - b) * (s - c));
    }

    @Override
    public double calPerimeter() {
        // TODO Auto-generated method stub
        return this.a + this.b + this.c;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return this.getName() + " A:" + this.a + " B:" + this.b + " C:" + this.c;
    }

}
